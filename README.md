# Sparks

Sparks is a Python library for dealing with word pluralization.

## Installation

Use the package manager [pip](https://pip.pypa.io/en/stable/) to install sparks.

```bash
pip install sparks
```

## Usage

```python
import sparks

# returns 'locals'
sparks.pluralize('local')

# returns 'geese'
sparks.pluralize('goose')

# returns 'server'
sparks.singularize('servers')
```

## Contributing

Pull requests are welcome. For major changes, please open an issue first
to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License

[MIT](https://choosealicense.com/licenses/mit/)
